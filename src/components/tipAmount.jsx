import React from 'react';

const TipAmount = ({ tipAmount }) => {
  return (
    <div>
      <h3>Tip Amount</h3>
      <h6>/ person</h6>

      <div>
        <span className="display-4">$</span>
        <span className="display-4">{tipAmount}</span>
      </div>
    </div>
  );
};

export default TipAmount;
