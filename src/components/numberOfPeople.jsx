import React from 'react';

const NoOfPeopleContainer = ({ noOfPeople, inputPeople }) => {
  return (
    <div>
      <h3>No of People</h3>
      <input
        type="number"
        placeholder="0"
        value={noOfPeople}
        onInput={inputPeople}
      />
    </div>
  );
};

export default NoOfPeopleContainer;
