import React from 'react';

const PercentButton = ({ value }) => {
  return (
    <>
      <button className="btn btn-success m-2">{value}</button>
    </>
  );
};

export default PercentButton;
