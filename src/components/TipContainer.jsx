import React from 'react';
import PercentButton from './percentButton';
import CustomInput from './customInput';

const tipPercentages = [5, 10, 15, 25, 50];

const TipContainer = ({
  changeButtonColorsAndGetPercent,
  customPercentValue,
  inputCustom,
}) => {
  return (
    <div>
      <h3>Select %</h3>
      <span onClick={changeButtonColorsAndGetPercent}>
        {tipPercentages.map((percent, index) => (
          <PercentButton key={index} value={percent} />
        ))}

        <CustomInput
          customPercentValue={customPercentValue}
          inputCustom={inputCustom}
        />
      </span>
    </div>
  );
};

export default TipContainer;
