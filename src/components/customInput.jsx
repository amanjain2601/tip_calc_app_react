import React from 'react';

const CustomInput = ({ customPercentValue, inputCustom }) => {
  return (
    <>
      <input
        type="number"
        value={customPercentValue}
        placeholder="Custom %"
        onInput={inputCustom}
      />
    </>
  );
};

export default CustomInput;
