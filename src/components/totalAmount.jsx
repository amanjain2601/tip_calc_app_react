import React from 'react';

const TotalAmount = ({ totalAmount }) => {
  return (
    <div>
      <h3>Total</h3>
      <h6>/ person</h6>

      <div>
        <span className="display-4">$</span>
        <span className="display-4">{totalAmount}</span>
      </div>
    </div>
  );
};

export default TotalAmount;
