import React from 'react';

const BillContainer = ({ billAmount, inputBill }) => {
  return (
    <div>
      <h3>Bill</h3>
      <input
        type="number"
        value={billAmount}
        placeholder="$0.00"
        onInput={inputBill}
      ></input>
    </div>
  );
};

export default BillContainer;
