import React, { Component } from 'react';
import TipAmount from './tipAmount';
import TotalAmount from './totalAmount';

class DisplayContainer extends Component {
  findTipAmount(amount, percent, people) {
    if (!isNaN(amount) && !isNaN(people) && !isNaN(percent)) {
      amount = Number(amount);
      people = Number(people);
      percent = Number(percent);

      if (amount > 0 && people > 0 && Number.isInteger(people)) {
        let ans = (amount * percent) / 100 / people;

        if (!isNaN(ans) && isFinite(ans)) {
          return Number(ans.toFixed(2));
        } else {
          return '0.00';
        }
      } else {
        return '0.00';
      }
    }
  }

  findTotalAmount = (amount, percent, people) => {
    let tipAmount = this.findTipAmount(amount, percent, people);

    if (tipAmount !== '0.00') {
      let amount2 = amount / Number(people);

      let ans = tipAmount + amount2;

      if (!isNaN(ans) && isFinite(ans)) {
        return Number(ans.toFixed(2));
      } else {
        return '0.00';
      }
    } else {
      return '0.00';
    }
  };

  render() {
    return (
      <div>
        <TipAmount
          tipAmount={this.findTipAmount(
            this.props.billAmount,
            this.props.percentValue,
            this.props.noOfPeople
          )}
        />
        <TotalAmount
          totalAmount={this.findTotalAmount(
            this.props.billAmount,
            this.props.percentValue,
            this.props.noOfPeople
          )}
        />
      </div>
    );
  }
}

export default DisplayContainer;
