import React, { Component } from 'react';
import BillContainer from './components/billContainer';
import TipContainer from './components/TipContainer';
import NoOfPeopleContainer from './components/numberOfPeople';
import DisplayContainer from './components/displayContainer';

class App extends Component {
  state = {
    billAmount: '',
    percentValue: '',
    noOfPeople: '',
    customPercentValue: '',
  };

  inputBill = (event) => {
    this.setState({
      billAmount: event.target.value,
    });
  };

  inputCustom = (event) => {
    this.setState({
      percentValue: event.target.value,
      customPercentValue: event.target.value,
    });
  };

  inputPeople = (event) => {
    this.setState({
      noOfPeople: event.target.value,
    });
  };

  reset = () => {
    this.setState({
      billAmount: '',
      percentValue: '',
      noOfPeople: '',
      customPercentValue: '',
    });
  };

  changeButtonColorsAndGetPercent = (event) => {
    if (event.target.tagName !== 'SPAN') {
      let ButtonArray = Array.from(
        event.currentTarget.getElementsByTagName('button')
      );

      ButtonArray.forEach((currentButton) => {
        if (currentButton.innerText !== event.target.innerText) {
          currentButton.className = 'btn btn-success m-2';
        }
      });

      if (event.target.tagName !== 'INPUT') {
        if (event.target.className === 'btn btn-success m-2') {
          this.setState(
            {
              percentValue: event.target.innerText,
            },
            () => {
              event.target.className = 'btn btn-danger m-2';
            }
          );
        } else {
          this.setState(
            {
              percentValue: '',
            },
            () => {
              event.target.className = 'btn btn-success m-2';
            }
          );
        }

        this.setState({
          customPercentValue: '',
        });
      } else {
        this.setState({
          percentValue: '',
        });
      }
    }
  };

  resetButtonColors(event) {
    if (
      event.target.classList.contains('reset') &&
      event.currentTarget.classList.contains('calcContainer')
    ) {
      let ButtonArray = Array.from(
        event.currentTarget.querySelectorAll('button')
      );

      ButtonArray = ButtonArray.slice(0, ButtonArray.length - 1);

      ButtonArray.forEach((button) => {
        button.className = 'btn btn-success m-2';
      });
    }
  }

  render() {
    return (
      <div onClick={this.resetButtonColors} className="calcContainer">
        <BillContainer
          billAmount={this.state.billAmount}
          inputBill={this.inputBill}
        />
        <TipContainer
          inputCustom={this.inputCustom}
          changeButtonColorsAndGetPercent={this.changeButtonColorsAndGetPercent}
          customPercentValue={this.state.customPercentValue}
        />
        <NoOfPeopleContainer
          noOfPeople={this.state.noOfPeople}
          inputPeople={this.inputPeople}
        />
        <DisplayContainer
          billAmount={this.state.billAmount}
          percentValue={this.state.percentValue}
          noOfPeople={this.state.noOfPeople}
        />
        <button className="btn btn-warning m-2 reset" onClick={this.reset}>
          Reset
        </button>
      </div>
    );
  }
}

export default App;
